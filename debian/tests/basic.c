#include <skalibs/random.h>

int main() {
  uint32_t max = 100;
  uint32_t n;
  size_t b = 100;
  char data[100];

  n = random_uint32(max);
  random_buf(data, b);
  return 0;
}
